package com.betvictor.test.web.rest;

import com.betvictor.test.Application;
import com.betvictor.test.domain.AppVersion;
import com.betvictor.test.repository.AppVersionRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the AppVersionResource REST controller.
 *
 * @see AppVersionResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class AppVersionResourceTest {

    private static final String DEFAULT_VERSION_FROM_MANIFEST = "AAAAA";
    private static final String UPDATED_VERSION_FROM_MANIFEST = "BBBBB";

    @Inject
    private AppVersionRepository appVersionRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restAppVersionMockMvc;

    private AppVersion appVersion;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AppVersionResource appVersionResource = new AppVersionResource();
        ReflectionTestUtils.setField(appVersionResource, "appVersionRepository", appVersionRepository);
        this.restAppVersionMockMvc = MockMvcBuilders.standaloneSetup(appVersionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        appVersion = new AppVersion();
        appVersion.setVersionFromManifest(DEFAULT_VERSION_FROM_MANIFEST);
    }

    @Test
    @Transactional
    public void createAppVersion() throws Exception {
        int databaseSizeBeforeCreate = appVersionRepository.findAll().size();

        // Create the AppVersion

        restAppVersionMockMvc.perform(post("/api/appVersions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(appVersion)))
                .andExpect(status().isCreated());

        // Validate the AppVersion in the database
        List<AppVersion> appVersions = appVersionRepository.findAll();
        assertThat(appVersions).hasSize(databaseSizeBeforeCreate + 1);
        AppVersion testAppVersion = appVersions.get(appVersions.size() - 1);
        assertThat(testAppVersion.getVersionFromManifest()).isEqualTo(DEFAULT_VERSION_FROM_MANIFEST);
    }

    @Test
    @Transactional
    public void getAllAppVersions() throws Exception {
        // Initialize the database
        appVersionRepository.saveAndFlush(appVersion);

        // Get all the appVersions
        restAppVersionMockMvc.perform(get("/api/appVersions"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(appVersion.getId().intValue())))
                .andExpect(jsonPath("$.[*].versionFromManifest").value(hasItem(DEFAULT_VERSION_FROM_MANIFEST.toString())));
    }

    @Test
    @Transactional
    public void getAppVersion() throws Exception {
        // Initialize the database
        appVersionRepository.saveAndFlush(appVersion);

        // Get the appVersion
        restAppVersionMockMvc.perform(get("/api/appVersions/{id}", appVersion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(appVersion.getId().intValue()))
            .andExpect(jsonPath("$.versionFromManifest").value(DEFAULT_VERSION_FROM_MANIFEST.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAppVersion() throws Exception {
        // Get the appVersion
        restAppVersionMockMvc.perform(get("/api/appVersions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAppVersion() throws Exception {
        // Initialize the database
        appVersionRepository.saveAndFlush(appVersion);

		int databaseSizeBeforeUpdate = appVersionRepository.findAll().size();

        // Update the appVersion
        appVersion.setVersionFromManifest(UPDATED_VERSION_FROM_MANIFEST);

        restAppVersionMockMvc.perform(put("/api/appVersions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(appVersion)))
                .andExpect(status().isOk());

        // Validate the AppVersion in the database
        List<AppVersion> appVersions = appVersionRepository.findAll();
        assertThat(appVersions).hasSize(databaseSizeBeforeUpdate);
        AppVersion testAppVersion = appVersions.get(appVersions.size() - 1);
        assertThat(testAppVersion.getVersionFromManifest()).isEqualTo(UPDATED_VERSION_FROM_MANIFEST);
    }

    @Test
    @Transactional
    public void deleteAppVersion() throws Exception {
        // Initialize the database
        appVersionRepository.saveAndFlush(appVersion);

		int databaseSizeBeforeDelete = appVersionRepository.findAll().size();

        // Get the appVersion
        restAppVersionMockMvc.perform(delete("/api/appVersions/{id}", appVersion.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<AppVersion> appVersions = appVersionRepository.findAll();
        assertThat(appVersions).hasSize(databaseSizeBeforeDelete - 1);
    }
}
