/* globals $ */
'use strict';

angular.module('actionMonitorApp')
    .directive('actionMonitorAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
