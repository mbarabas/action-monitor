/* globals $ */
'use strict';

angular.module('actionMonitorApp')
    .directive('actionMonitorAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
