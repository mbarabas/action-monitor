 'use strict';

angular.module('actionMonitorApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-actionMonitorApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-actionMonitorApp-params')});
                }
                return response;
            }
        };
    });
