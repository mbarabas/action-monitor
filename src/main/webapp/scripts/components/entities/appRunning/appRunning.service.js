'use strict';

angular.module('actionMonitorApp')
    .factory('AppRunning', function ($resource, DateUtils) {
        return $resource('api/appRunnings/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
