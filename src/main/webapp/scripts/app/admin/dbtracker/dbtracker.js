angular.module('actionMonitorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('dbtracker', {
                parent: 'admin',
                url: '/dbtracker',
                data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'dbtracker.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/admin/dbtracker/dbtracker.html',
                        controller: 'DBTrackerController'
                    }
                },
                resolve: {
                    mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('dbtracker');
                        return $translate.refresh();
                    }]
                },
                onEnter: function(DBTracker) {
                    DBTracker.subscribe();
                },
                onExit: function(DBTracker) {
                    DBTracker.unsubscribe();
                },
            });
    });
