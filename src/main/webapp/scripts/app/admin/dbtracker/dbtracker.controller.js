angular.module('actionMonitorApp')
    .controller('DBTrackerController', function ($scope, $cookies, $http, DBTracker) {
        // This controller uses a Websocket connection to receive database activities in real-time.
    	DBTracker.connect();
    	
        $scope.activities = [];
        DBTracker.receive().then(null, null, function(activity) {
            showActivity(activity);
        });

        function showActivity(activity) {
            var existingActivity = false;
//            for (var index = 0; index < $scope.activities.length; index++) {
//                if($scope.activities[index].sessionId == activity.sessionId) {
//                    existingActivity = true;
//                    $scope.activities[index] = activity;
//                }
//            }
            if (!existingActivity) {
                $scope.activities.push(activity);
            }
        };
    });
