'use strict';

angular.module('actionMonitorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('appVersion', {
                parent: 'entity',
                url: '/appVersions',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'actionMonitorApp.appVersion.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/appVersion/appVersions.html',
                        controller: 'AppVersionController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('appVersion');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('appVersion.detail', {
                parent: 'entity',
                url: '/appVersion/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'actionMonitorApp.appVersion.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/appVersion/appVersion-detail.html',
                        controller: 'AppVersionDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('appVersion');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AppVersion', function($stateParams, AppVersion) {
                        return AppVersion.get({id : $stateParams.id});
                    }]
                }
            })
            .state('appVersion.new', {
                parent: 'appVersion',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/appVersion/appVersion-dialog.html',
                        controller: 'AppVersionDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    versionFromManifest: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('appVersion', null, { reload: true });
                    }, function() {
                        $state.go('appVersion');
                    })
                }]
            })
            .state('appVersion.edit', {
                parent: 'appVersion',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/appVersion/appVersion-dialog.html',
                        controller: 'AppVersionDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['AppVersion', function(AppVersion) {
                                return AppVersion.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('appVersion', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
