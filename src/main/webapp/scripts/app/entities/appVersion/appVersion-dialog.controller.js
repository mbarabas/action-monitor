'use strict';

angular.module('actionMonitorApp').controller('AppVersionDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'AppVersion',
        function($scope, $stateParams, $modalInstance, entity, AppVersion) {

        $scope.appVersion = entity;
        $scope.load = function(id) {
            AppVersion.get({id : id}, function(result) {
                $scope.appVersion = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('actionMonitorApp:appVersionUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.appVersion.id != null) {
                AppVersion.update($scope.appVersion, onSaveFinished);
            } else {
                AppVersion.save($scope.appVersion, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
