'use strict';

angular.module('actionMonitorApp')
    .controller('AppVersionController', function ($scope, AppVersion) {
        $scope.appVersions = [];
        $scope.loadAll = function() {
            AppVersion.query(function(result) {
               $scope.appVersions = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AppVersion.get({id: id}, function(result) {
                $scope.appVersion = result;
                $('#deleteAppVersionConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AppVersion.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAppVersionConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.appVersion = {
                versionFromManifest: null,
                id: null
            };
        };
    });
