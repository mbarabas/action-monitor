'use strict';

angular.module('actionMonitorApp')
    .controller('AppVersionDetailController', function ($scope, $rootScope, $stateParams, entity, AppVersion) {
        $scope.appVersion = entity;
        $scope.load = function (id) {
            AppVersion.get({id: id}, function(result) {
                $scope.appVersion = result;
            });
        };
        var unsubscribe = $rootScope.$on('actionMonitorApp:appVersionUpdate', function(event, result) {
            $scope.appVersion = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
