'use strict';

angular.module('actionMonitorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('appRunning', {
                parent: 'entity',
                url: '/appRunnings',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'actionMonitorApp.appRunning.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/appRunning/appRunnings.html',
                        controller: 'AppRunningController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('appRunning');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('appRunning.detail', {
                parent: 'entity',
                url: '/appRunning/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'actionMonitorApp.appRunning.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/appRunning/appRunning-detail.html',
                        controller: 'AppRunningDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('appRunning');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'AppRunning', function($stateParams, AppRunning) {
                        return AppRunning.get({id : $stateParams.id});
                    }]
                }
            })
            .state('appRunning.new', {
                parent: 'appRunning',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/appRunning/appRunning-dialog.html',
                        controller: 'AppRunningDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('appRunning', null, { reload: true });
                    }, function() {
                        $state.go('appRunning');
                    })
                }]
            })
            .state('appRunning.edit', {
                parent: 'appRunning',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/appRunning/appRunning-dialog.html',
                        controller: 'AppRunningDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['AppRunning', function(AppRunning) {
                                return AppRunning.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('appRunning', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
