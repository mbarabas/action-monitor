'use strict';

angular.module('actionMonitorApp').controller('AppRunningDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'AppRunning',
        function($scope, $stateParams, $modalInstance, entity, AppRunning) {

        $scope.appRunning = entity;
        $scope.load = function(id) {
            AppRunning.get({id : id}, function(result) {
                $scope.appRunning = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('actionMonitorApp:appRunningUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.appRunning.id != null) {
                AppRunning.update($scope.appRunning, onSaveFinished);
            } else {
                AppRunning.save($scope.appRunning, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
