'use strict';

angular.module('actionMonitorApp')
    .controller('AppRunningDetailController', function ($scope, $rootScope, $stateParams, entity, AppRunning) {
        $scope.appRunning = entity;
        $scope.load = function (id) {
            AppRunning.get({id: id}, function(result) {
                $scope.appRunning = result;
            });
        };
        var unsubscribe = $rootScope.$on('actionMonitorApp:appRunningUpdate', function(event, result) {
            $scope.appRunning = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
