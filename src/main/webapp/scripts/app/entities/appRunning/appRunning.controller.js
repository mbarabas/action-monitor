'use strict';

angular.module('actionMonitorApp')
    .controller('AppRunningController', function ($scope, AppRunning) {
        $scope.appRunnings = [];
        $scope.loadAll = function() {
            AppRunning.query(function(result) {
               $scope.appRunnings = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            AppRunning.get({id: id}, function(result) {
                $scope.appRunning = result;
                $('#deleteAppRunningConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            AppRunning.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteAppRunningConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.appRunning = {
                id: null
            };
        };
    });
