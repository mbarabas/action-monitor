package com.betvictor.test.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A AppVersion.
 */
@Entity
@Table(name = "app_version")
public class AppVersion implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "version_from_manifest")
    private String versionFromManifest;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVersionFromManifest() {
        return versionFromManifest;
    }

    public void setVersionFromManifest(String versionFromManifest) {
        this.versionFromManifest = versionFromManifest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AppVersion appVersion = (AppVersion) o;

        if ( ! Objects.equals(id, appVersion.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "AppVersion{" +
            "id=" + id +
            ", versionFromManifest='" + versionFromManifest + "'" +
            '}';
    }
}
