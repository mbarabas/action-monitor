package com.betvictor.test.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A AppRunning.
 */
@Entity
@Table(name = "app_running")
public class AppRunning implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AppRunning appRunning = (AppRunning) o;

        if ( ! Objects.equals(id, appRunning.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "AppRunning{" +
            "id=" + id +
            '}';
    }
}
