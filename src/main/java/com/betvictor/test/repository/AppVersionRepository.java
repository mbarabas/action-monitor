package com.betvictor.test.repository;

import com.betvictor.test.domain.AppVersion;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the AppVersion entity.
 */
public interface AppVersionRepository extends JpaRepository<AppVersion,Long> {

}
