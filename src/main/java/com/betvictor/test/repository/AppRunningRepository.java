package com.betvictor.test.repository;

import com.betvictor.test.domain.AppRunning;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the AppRunning entity.
 */
public interface AppRunningRepository extends JpaRepository<AppRunning,Long> {

}
