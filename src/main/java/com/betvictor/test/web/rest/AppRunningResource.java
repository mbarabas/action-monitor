package com.betvictor.test.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.betvictor.test.domain.AppRunning;
import com.betvictor.test.repository.AppRunningRepository;
import com.betvictor.test.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AppRunning.
 */
@RestController
@RequestMapping("/api")
public class AppRunningResource {

    private final Logger log = LoggerFactory.getLogger(AppRunningResource.class);

    /**
     * GET  /appRunning -> get all the appRunning.
     */
    @RequestMapping(value = "/appRunning",
        method = RequestMethod.GET,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public String getAllappRunning() {
        log.debug("REST request to get all appRunning");
        return "You are logged in, and the APP is running!";
    }

}
