package com.betvictor.test.web.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.betvictor.test.aop.notify.NotifyClients;
import com.betvictor.test.domain.AppVersion;
import com.betvictor.test.repository.AppVersionRepository;
import com.betvictor.test.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;

/**
 * REST controller for managing AppVersion.
 */
@RestController
@RequestMapping("/api")
public class AppVersionResource {

    private final Logger log = LoggerFactory.getLogger(AppVersionResource.class);

    @Inject
    private AppVersionRepository appVersionRepository;

    /**
     * POST  /appVersions -> Create a new appVersion.
     */
    @NotifyClients
    @RequestMapping(value = "/appVersions",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AppVersion> createAppVersion(@RequestBody AppVersion appVersion) throws URISyntaxException {
        log.debug("REST request to save AppVersion : {}", appVersion);
        if (appVersion.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new appVersion cannot already have an ID").body(null);
        }
        AppVersion result = appVersionRepository.save(appVersion);
        String cmd = "INSERT";
        return ResponseEntity.created(new URI("/api/appVersions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("appVersion", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /appVersions -> Updates an existing appVersion.
     */
    @RequestMapping(value = "/appVersions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AppVersion> updateAppVersion(@RequestBody AppVersion appVersion) throws URISyntaxException {
        log.debug("REST request to update AppVersion : {}", appVersion);
        if (appVersion.getId() == null) {
            return createAppVersion(appVersion);
        }
        AppVersion result = appVersionRepository.save(appVersion);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("appVersion", appVersion.getId().toString()))
            .body(result);
    }

    /**
     * GET  /appVersions -> get all the appVersions.
     */
    @RequestMapping(value = "/appVersions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<AppVersion> getAllAppVersions() {
        log.debug("REST request to get all AppVersions");
        return appVersionRepository.findAll();
    }

    /**
     * GET  /appVersions/:id -> get the "id" appVersion.
     */
    @RequestMapping(value = "/appVersions/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AppVersion> getAppVersion(@PathVariable Long id) {
        log.debug("REST request to get AppVersion : {}", id);
        return Optional.ofNullable(appVersionRepository.findOne(id))
            .map(appVersion -> new ResponseEntity<>(
                appVersion,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /appVersions/:id -> delete the "id" appVersion.
     */
    @RequestMapping(value = "/appVersions/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAppVersion(@PathVariable Long id) {
        log.debug("REST request to delete AppVersion : {}", id);
        appVersionRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("appVersion", id.toString())).build();
    }
    
    /**
     * GET  /appVersions -> get all the appVersions.
     */
    @RequestMapping(value = "/appVersion/manifest",
        method = RequestMethod.GET,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public String getAppVersionFromManifest() {
        log.debug("REST request to get AppVersion from MANIFEST file");
        String version = null;
        
//        Properties properties = new Properties();
//        InputStream resourceAsStream = this.getClass().getResourceAsStream("/project.properties");
//
//        try {
//        	properties.load(resourceAsStream);
//        }
//        catch (Exception e) {
//        	log.error("error", e);
//        }
//	      
//        version = properties.getProperty("version");
        
        version = getClass().getPackage().getImplementationVersion();  
        if (version == null) {
            Properties prop = new Properties();
            try {
                prop.load(this.getClass().getResourceAsStream("/META-INF/MANIFEST.MF"));
                version = prop.getProperty("Implementation-Version");
            } catch (IOException e) {
                log.error(e.toString());
            }
        }
        log.info("App version "+version);        
        
        
        return version;
    }
}
