/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.betvictor.test.web.rest.dto;
