package com.betvictor.test.web.websocket;

import com.betvictor.test.security.SecurityUtils;
import com.betvictor.test.web.websocket.dto.DBActivityDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import javax.inject.Inject;
import java.security.Principal;
import java.util.Calendar;

import static com.betvictor.test.config.WebsocketConfiguration.IP_ADDRESS;

@Controller
public class DBActivityService implements ApplicationListener<SessionDisconnectEvent> {

    private static final Logger log = LoggerFactory.getLogger(DBActivityService.class);

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Inject
    SimpMessageSendingOperations messagingTemplate;

//    @SubscribeMapping("/topic/dbactivity")
//    @SendTo("/topic/dbtracker")
//    public DBActivityDTO sendActivity(@Payload DBActivityDTO dbActivityDTO, StompHeaderAccessor stompHeaderAccessor, Principal principal) {
//        dbActivityDTO.setValue(SecurityUtils.getCurrentUserLogin());
//        dbActivityDTO.setValue(principal.getName());
//        dbActivityDTO.setSessionId(stompHeaderAccessor.getSessionId());
//        dbActivityDTO.setID(stompHeaderAccessor.getSessionAttributes().get(IP_ADDRESS).toString());
//        Instant instant = Instant.ofEpochMilli(Calendar.getInstance().getTimeInMillis());
//        dbActivityDTO.setTime(dateTimeFormatter.format(ZonedDateTime.ofInstant(instant, ZoneOffset.systemDefault())));
//        log.debug("Sending db tracking data {}", dbActivityDTO);
//        return dbActivityDTO;
//    }

    @Override
    public void onApplicationEvent(SessionDisconnectEvent event) {
        DBActivityDTO dbActivityDTO = new DBActivityDTO();
        dbActivityDTO.setSessionId(event.getSessionId());
        dbActivityDTO.setCommand("logout");
        messagingTemplate.convertAndSend("/topic/dbtracker", dbActivityDTO);
    }
}
