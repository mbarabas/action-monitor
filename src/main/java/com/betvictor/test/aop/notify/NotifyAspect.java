package com.betvictor.test.aop.notify;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;

import javax.inject.Inject;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import com.betvictor.test.config.Constants;
import com.betvictor.test.domain.AppVersion;
import com.betvictor.test.web.websocket.dto.DBActivityDTO;

@Aspect
public class NotifyAspect {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private SimpMessagingTemplate template;

    private static final String WEBSOCKET_TOPIC = "/topic/dbtracker";
    
    private static String cmd;
    
    @Inject
    private Environment env;

//    @Pointcut("within(com.betvictor.test.repository..*) || within(com.betvictor.test.service..*) || within(com.betvictor.test.web.rest..*)")
//    public void notifyPointcut() {
//    }
    
    @Pointcut("notifyOnCreate() || notifyOnUpdate()")
    public void notifyPointcut() {
    	
    }
    
    @Pointcut("execution(* com.betvictor.test.web.rest..*.create*(..))")
    public void notifyOnCreate() {
    	cmd = "CREATE";
    }
    
    @Pointcut("execution(* com.betvictor.test.web.rest..*.update*(..))")
    public void notifyOnUpdate() {
    	cmd = "UPDATE";
    }
    
    @Pointcut("execution(* com.betvictor.test.web.rest..*.delete*(..))")
    public void notifyOnDelete() {
    	cmd = "DELETE";
    }
    
    @AfterThrowing(pointcut = "notifyOnCreate() || notifyOnUpdate()", throwing = "e")
    public void notifyAfterThrowing(JoinPoint joinPoint, Throwable e) {
        if (env.acceptsProfiles(Constants.SPRING_PROFILE_DEVELOPMENT)) {
            log.error("Exception in {}.{}() with cause = {}", joinPoint.getSignature().getDeclaringTypeName(),
                joinPoint.getSignature().getName(), e.getCause(), e);
        } else {
            log.error("Exception in {}.{}() with cause = {}", joinPoint.getSignature().getDeclaringTypeName(),
                joinPoint.getSignature().getName(), e.getCause());
        }
    }

    @Around("notifyPointcut()")
    public Object notifyAround(ProceedingJoinPoint joinPoint) throws Throwable {
        if (log.isDebugEnabled()) {
            log.debug("Enter: {}.{}() with argument[s] = {}", joinPoint.getSignature().getDeclaringTypeName(),
                joinPoint.getSignature().getName(), Arrays.toString(joinPoint.getArgs()));
        }
        try {
            Object result = joinPoint.proceed();
            if (log.isDebugEnabled()) {
                log.debug("Exit: {}.{}() with result = {}", joinPoint.getSignature().getDeclaringTypeName(),
                    joinPoint.getSignature().getName(), result);
            }
            return result;
        } catch (IllegalArgumentException e) {
            log.error("Illegal argument: {} in {}.{}()", Arrays.toString(joinPoint.getArgs()),
                    joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());

            throw e;
        }
    }


    @After("notifyPointcut() &&" +
    		"args(result)") 
    public void notifyClients(JoinPoint joinPoint, AppVersion result) throws Throwable {
//    	AppVersion result = (AppVersion)joinPoint.getTarget();
    	if (joinPoint.getSignature().getName().toLowerCase().contains("CREATE".toLowerCase())) {
    		cmd = "CREATE";
    	}
    	else if (joinPoint.getSignature().getName().toLowerCase().contains("UPDATE".toLowerCase())) {
    		cmd = "UPDATE";
    	}
    	else if (joinPoint.getSignature().getName().toLowerCase().contains("DELETE".toLowerCase())) {
    		cmd = "DELETE";
    	}
    	
    	DBActivityDTO dbActivityDTO = new DBActivityDTO();
    	dbActivityDTO.setSessionId(result.getId().toString());
    	dbActivityDTO.setID(result.getId().toString());
    	dbActivityDTO.setValue(result.getVersionFromManifest());
    	dbActivityDTO.setCommand(cmd);
    	Instant instant = Instant.ofEpochMilli(Calendar.getInstance().getTimeInMillis());
        dbActivityDTO.setTime(dateTimeFormatter.format(ZonedDateTime.ofInstant(instant, ZoneOffset.systemDefault())));
        template.convertAndSend(WEBSOCKET_TOPIC, dbActivityDTO);
    }
  
    
}
